from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Article, Author
from .serializers import ArticleSerializer, AuthorSerializer


class OneAuthorView(APIView):
    def get(self, request, pk):
        author  = get_object_or_404(Author.objects.all(), pk=pk)
        # the many param informs the serializer that it will be serializing more than a single article.
        serializer = AuthorSerializer(author)
        return Response({"author": serializer.data})

class ArticleView(APIView):
    def get(self, request):
        articles = Article.objects.all()
        # the many param informs the serializer that it will be serializing more than a single article.
        serializer = ArticleSerializer(articles, many=True)
        return Response({"articles": serializer.data})

    def post(self, request):
        article = request.data.get('article')
        # Create an article from the above data
        serializer = ArticleSerializer(data=article)
        if serializer.is_valid(raise_exception=True):
            article_saved = serializer.save()
        return Response({"success": "Article '{}' created successfully".format(article_saved.title)})